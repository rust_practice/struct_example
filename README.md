# Structs, Impls, and Associated Functions
Rust has the `struct` type which is the same in C, with some fun bonus features.

To declare a struct, simply use the `struct` keyword:
```rust
struct Rectangle {
    width: u32,
    height: u32,
}
```
To create an instance of it:
```rust
let rect1 = Rectangle {
    width: 30,
    height: 50,
};
```

## Impl
The `impl` keyword stands for "Implementation", and is how you tie methods to structs. For instance, we can create an `impl` for the `Rectangle` struct, and tie a method, `area()` to it:
```rust
impl Rectangle{
    fn area(&self) -> u32 {
        self.width * self.height
    }
}
```
`area()` takes `&self` as a parameter because it is called by an instansiated `Rectangle` object. For instance:
```rust
let rect1 = Rectangle {
            width: 3,
            height: 5,
        };
let ans = rect1.area();
```

## Associated Functions
Associated functions are functions that are tied to an `impl` but do not have a reference to `self`, therefore aren't methods. This is useful for constructors and such. Here is an example:
```rust
impl Rectangle {
    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

Rectangle::square(45);
```
Since they are not tied to `self`, you have to call them off the struct.