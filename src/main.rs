struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle{
    fn area(&self) -> u32 {
        self.width * self.height
    }

    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}


fn main() {
    let rect1 = Rectangle {
        width: 30,
        height: 50,
    };

    println!("The are of the rectangle is {} square pixels",
rect1.area());
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_area() {
        let rect1 = Rectangle {
            width: 3,
            height: 5,
        };
        let ans = rect1.area();
        assert_eq!(ans, (rect1.width * rect1.height));
    }

    #[test]
    fn test_can_hold() {
        let rect1 = Rectangle {
            width: 3,
            height: 5,
        };
        let rect2 = Rectangle {
            width: 4,
            height: 6,
        };

        let ans = rect1.can_hold(&rect2);
        assert_eq!(ans, false);
        let ans = rect2.can_hold(&rect1);
        assert_eq!(ans, true);
    }

    #[test]
    fn test_square() {
        // square is an 'associated function' so you have to call it on the type not self
        let ans = Rectangle::square(4);
        assert_eq!(ans.height, ans.width);

    }
}